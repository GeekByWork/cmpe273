import logging, json, requests, time

from spyne import Application, srpc, ServiceBase, Iterable, UnsignedInteger, \
    String, ComplexModel, Integer, Array, Long, Decimal

from spyne.protocol.json import JsonDocument
from spyne.protocol.http import HttpRpc
from spyne.server.wsgi import WsgiApplication

from datetime import datetime

class EventTypeCount(ComplexModel):
    slot1start = datetime.strptime("1/1/16 12:01 AM", "%m/%d/%y %I:%M %p").time() 
    slot1end = datetime.strptime("1/1/16 03:00 AM", "%m/%d/%y %I:%M %p").time()
    slot2start = datetime.strptime("1/1/16 03:01 AM", "%m/%d/%y %I:%M %p").time()
    slot2end = datetime.strptime("1/1/16 06:00 AM", "%m/%d/%y %I:%M %p").time()
    slot3start = datetime.strptime("1/1/16 06:01 AM", "%m/%d/%y %I:%M %p").time()
    slot3end = datetime.strptime("1/1/16 09:00 AM", "%m/%d/%y %I:%M %p").time()
    slot4start = datetime.strptime("1/1/16 09:01 AM", "%m/%d/%y %I:%M %p").time()
    slot4end = datetime.strptime("1/1/16 12:00 PM", "%m/%d/%y %I:%M %p").time()
    slot5start = datetime.strptime("1/1/16 12:01 PM", "%m/%d/%y %I:%M %p").time()
    slot5end = datetime.strptime("1/1/16 03:00 PM", "%m/%d/%y %I:%M %p").time()
    slot6start = datetime.strptime("1/1/16 03:01 PM", "%m/%d/%y %I:%M %p").time()
    slot6end = datetime.strptime("1/1/16 06:00 PM", "%m/%d/%y %I:%M %p").time()
    slot7start = datetime.strptime("1/1/16 06:01 PM", "%m/%d/%y %I:%M %p").time()
    slot7end = datetime.strptime("1/1/16 09:00 PM", "%m/%d/%y %I:%M %p").time()
    slot8start = datetime.strptime("1/1/16 09:01 PM", "%m/%d/%y %I:%M %p").time()
    slot8end = datetime.strptime("1/1/16 12:00 AM", "%m/%d/%y %I:%M %p") .time()
    streets = {}    

result = {
    "total_crime" : 0,
    "the_most_dangerous_streets" : [],
    "crime_type_count" : {
        "Assault" : 0,
        "Arrest" : 0,
        "Burglary" : 0,
        "Robbery" : 0,
        "Theft" : 0,
        "Other" : 0
    },
    "event_time_count" : {
        "12:01am-3am" : 0,
        "3:01am-6am" : 0,
        "6:01am-9am" : 0,
        "9:01am-12noon" : 0,
        "12:01pm-3pm" : 0,
        "3:01pm-6pm" : 0,
        "6:01pm-9pm" : 0,
        "9:01pm-12midnight" : 0
    } 
}

class CrimeCheckService(ServiceBase):
    @srpc(Decimal, Decimal, Decimal, _returns=String)
    def checkcrime(lat, lon, radius):        
        payload = {'lat': lat, 'lon': lon,'radius': radius, 'key':'.'}
        r = requests.get('https://api.spotcrime.com/crimes.json', params=payload)        
        resp = json.loads(r.text)        
        etc = EventTypeCount()        
        for c in resp["crimes"]:            
            result["total_crime"] += 1
            if c['type'] == "Assault":
                result["crime_type_count"]["Assault"] += 1
            elif c['type'] == "Arrest":
                    result["crime_type_count"]["Arrest"] += 1
            elif c['type'] == "Burglary":
                    result["crime_type_count"]["Burglary"] += 1
            elif c['type'] =="Robbery":
                    result["crime_type_count"]["Robbery"] += 1
            elif c['type'] == "Theft":
                    result["crime_type_count"]["Theft"] += 1
            elif c['type'] == "Other":
                    result["crime_type_count"]["Other"] += 1
            
            crimeTime = datetime.strptime(c['date'], "%m/%d/%y %I:%M %p").time()
            
            if (crimeTime>(etc.slot1start) and crimeTime<etc.slot1end) or crimeTime==etc.slot1start or crimeTime==etc.slot1end:
                result["event_time_count"]["12:01am-3am"] += 1
            elif (crimeTime>etc.slot2start and crimeTime<etc.slot2end) or crimeTime==etc.slot2start or crimeTime==etc.slot2end:
                    result["event_time_count"]["3:01am-6am"] += 1
            elif (crimeTime>etc.slot3start and crimeTime<etc.slot3end) or crimeTime==etc.slot3start or crimeTime==etc.slot3end:
                    result["event_time_count"]["6:01am-9am"] += 1
            elif (crimeTime>etc.slot4start and crimeTime<etc.slot4end) or crimeTime==etc.slot4start or crimeTime==etc.slot4end:
                    result["event_time_count"]["9:01am-12noon"] += 1
            elif (crimeTime>etc.slot5start and crimeTime<etc.slot5end) or crimeTime==etc.slot5start or crimeTime==etc.slot5end:
                    result["event_time_count"]["12:01pm-3pm"] += 1
            elif (crimeTime>etc.slot6start and crimeTime<etc.slot6end) or crimeTime==etc.slot6start or crimeTime==etc.slot6end:
                    result["event_time_count"]["3:01pm-6pm"] += 1
            elif (crimeTime>etc.slot7start and crimeTime<etc.slot7end) or crimeTime==etc.slot7start or crimeTime==etc.slot7end:
                    result["event_time_count"]["6:01pm-9pm"] += 1
            elif (crimeTime>etc.slot8start and crimeTime<etc.slot8end) or crimeTime==etc.slot8start or crimeTime==etc.slot8end:
                    result["event_time_count"]["9:01pm-12midnight"] += 1
            
            pStreets = []
            if c["address"].find(' & ')!=-1:
                street = c["address"].split(' & ')                
                for st in street:
                    pStreets.append(st)
            else:
                pStreets.append(c["address"])
            for st in pStreets:
                if st.find('BLOCK OF')!=-1:
                    st = st.split('BLOCK OF ')[1]
                if st.rfind('BLOCK ')!=-1:
                    st =st[st.rfind('BLOCK ') + 6:]
                etc.streets[st] = etc.streets.get(st, 0) + 1
        st1 = ""
        st2 = ""
        st3 = ""
        i = 0
        arr = etc.streets
        for elem in etc.streets:
            if i==0:
                st1 = elem
                i += 1
                continue
            if i==1:
                st2 = elem
                if arr[st2]>arr[st1]:
                    temp = st2
                    st2 = st1
                    st1 = temp
                i += 1
                continue
            if i==2:
                st3 = elem
                if arr[st3] > arr[st2]:
                    temp = st3
                    st3 = st2
                    st2 = temp
                if arr[st2] > arr[st1]:
                    temp = st2
                    st2 = st1
                    st1 = temp
                i += 1
                continue
            if arr[elem] > arr[st3]:
                st3 = elem
                if arr[st3] > arr[st2]:
                    temp = st3
                    st3 = st2
                    st2 = temp
                if arr[st2] > arr[st1]:
                    temp = st2
                    st2 = st1
                    st1 = temp
                i += 1
                continue
            i += 1
        result["the_most_dangerous_streets"] = [st1, st2, st3]
        return result

if __name__ == '__main__':
    from wsgiref.simple_server import make_server

    logging.basicConfig(level=logging.DEBUG)
    application = Application([CrimeCheckService], 'spyne.examples.hello.http',
        in_protocol=HttpRpc(validator='soft'),
        out_protocol=JsonDocument(ignore_wrappers=True),
    )

    wsgi_application = WsgiApplication(application)

    server = make_server('127.0.0.1', 8000, wsgi_application)

    logging.info("listening to http://127.0.0.1:8000")
    logging.info("wsdl is at: http://localhost:8000/?wsdl")

    server.serve_forever()