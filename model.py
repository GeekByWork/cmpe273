from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app=Flask(__name__)

DATABASE = 'test'
PASSWORD = 'p@ssw0rd123'
USER = 'root'
HOSTNAME = 'db'
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://%s:%s@%s/%s'%(USER, PASSWORD, HOSTNAME, DATABASE)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True

db=SQLAlchemy(app)

class CreateDB():
    def __init__(self):
        import sqlalchemy
        engine = sqlalchemy.create_engine('mysql://%s:%s@%s' % (USER, PASSWORD, HOSTNAME))  # connect to server
        engine.execute("CREATE DATABASE IF NOT EXISTS %s " % (DATABASE))  # create db
        db.create_all()
    
class Orders(db.Model):
    __tablename__='Orders'
    id=db.Column('ID',db.Integer, primary_key=True)    
    category=db.Column('CATEGORY',db.String(45))
    description = db.Column('DESCRIPTION', db.String(250))
    link = db.Column('LINK', db.String(250))
    estimatedCost = db.Column('ESTIMATED_COST', db.String(250))
    submitDate = db.Column('SUBMIT_DATE', db.String(45))
    status = db.Column('STATUS', db.String(45))
    decisionDate = db.Column('DECISION_DATE',db.String(45))
    customerName=db.Column('CUSTOMER_NAME',db.String(45))
    customerEmail=db.Column('CUSTOMER_EMAIL',db.String(45))


    def __init__(self, customerName, customerEmail, category, description, link, estimatedCost, submitDate, status, decisionDate):        
        self.category=category
        self.description=description
        self.link=link
        self.estimatedCost=estimatedCost
        self.submitDate=submitDate
        self.status=status
        self.decisionDate=decisionDate
        self.customerName=customerName
        self.customerEmail=customerEmail
