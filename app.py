from flask import Flask, jsonify, Response, json, request, logging

from model import Orders, db, CreateDB

app = Flask(__name__)

@app.route('/')
def index():
    return 'Hello World!'


@app.route('/v1/expenses/<int:orderId>', methods=['GET', 'PUT', 'DELETE'])
def getExpenses(orderId):
    try:

        if request.method == 'PUT':
            order = Orders.query.filter_by(id=orderId).first()
            if None != order:
                myjson = request.get_json(force=True)

                if None != myjson["estimated_costs"]:
                    order.estimatedCost = myjson["estimated_costs"]

                db.session.commit()

                return "Accepted", 202
            else:
                jsonString = {"message": "No Rows Found"}
                return jsonify(jsonString), 404

        elif request.method == 'GET':
            order = Orders.query.filter_by(id=orderId).first()
            if None != order:
                jsonString = {
                    "id": order.id,
                    "name": order.customerName,
                    "email": order.customerEmail,
                    "category": order.category,
                    "description": order.description,
                    "link": order.link,
                    "estimated_costs": order.estimatedCost,
                    "submit_date": order.submitDate,
                    "status": order.status,
                    "decision_date": ""}

                response = Response(response=json.dumps(jsonString),
                                    status=200, mimetype="application/json")
                return response

            else:
                jsonString = {"message": "No Rows Found"}
                return jsonify(jsonString), 404

        elif request.method == 'DELETE':
            order = Orders.query.get(orderId)
            if None != order:
                db.session.delete(order)
                db.session.commit()
                return jsonify({"message": "Rows Deleted"}), 204
            else:
                jsonString = {"message": "No Rows Found"}
                return jsonify(jsonString), 404
    except Exception as e:
        return jsonify({"message": e.message})


@app.route('/v1/expenses', methods=['POST', 'GET'])
def expenses():
    app.logger.info('hello')
    CreateDB()    
    try:
        if request.method == 'POST':
            myjson = request.get_json(force=True)
            name = myjson['name']
            email = myjson['email']
            category = myjson['category']
            description = myjson['description']
            link = myjson['link']
            estimated_costs = myjson['estimated_costs']
            submit_date = myjson['submit_date']
            order = Orders(name, email, category, description, link, estimated_costs, submit_date, 'PENDING','')
            try:
                db.session.add(order)
                db.session.commit()
            except Exception as e:
                db.session.rollback()
                return jsonify({"Session message": e.message})

            jsonString = {"id": order.id,
                          "name": order.customerName,
                          "email": order.customerEmail,
                          "category": order.category,
                          "description": order.description,
                          "link": order.link,
                          "estimated_costs": order.estimatedCost,
                          "submit_date": order.submitDate,
                          "status": order.status,
                          "decision_date": ""}   
            response = Response(response = json.dumps(jsonString),
                                status = 201, \
                                mimetype="application/json")
            return response
        else:

            orders = Orders.query.all()
            if None != orders:
                orders_dict = {}
                for order in orders:
                    orders_dict[order.id] = {
                        "id": order.id,
                        "name": order.customerName,
                        "email": order.customerEmail,
                        "category": order.category,
                        "description": order.description,
                        "link": order.link,
                        "estimated_costs": order.estimatedCost,
                        "submit_date": order.submitDate,
                        "status": order.status,
                        "decision_date": ""
                    }
                return json.dumps(orders_dict), 200
            else:
                return jsonify({"message": "no row found"}), 404
    except Exception as e:
        return jsonify({"message": e.message})


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)