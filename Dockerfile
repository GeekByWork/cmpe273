FROM python:2.7
MAINTAINER Umang Saxena "umang.saxena@sjsu.edu"
COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt
ENTRYPOINT ["python"]
CMD ["app.py"]